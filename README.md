# UjPjsP2Tensorflow

## Usage

- `npm install`
- `ng serve`
- open http://localhost:4200

## Keras Convolutional Neural Network model in TensorFlow

Keras CNN model was converted with tensorflowjs_converter:

- `pip install tensorflowjs`
- `tensorflowjs_converter --input_format keras keras/cnn.h5 src/assets`
