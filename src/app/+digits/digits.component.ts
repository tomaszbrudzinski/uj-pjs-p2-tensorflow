import {Component, OnInit, ViewChild} from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import {DrawableDirective} from './directives/drawable.directive';

@Component({
  selector: 'app-digits',
  templateUrl: './digits.component.html',
  styleUrls: ['./digits.component.scss']
})
export class DigitsComponent implements OnInit {

  @ViewChild(DrawableDirective, {static: false}) canvas: DrawableDirective;
  public loading: boolean = true;
  public predictedNumber: number;
  private model: tf.LayersModel;
  private predictions: any[];

  public async ngOnInit(): Promise<void> {
    this.model = await tf.loadLayersModel('/assets/model.json');
    this.loading = false;
  }

  public async predict(imageData: ImageData): Promise<void> {

    await tf.tidy(() => {
      let img: tf.Tensor3D = tf.browser.fromPixels(imageData, 1);
      img = img.reshape([1, 28, 28, 1] as any);
      img = tf.cast(img, 'float32');

      const output: tf.Tensor = this.model.predict(img) as any;

      this.predictions = Array.from(output.dataSync());
      this.predictedNumber = this.predictions.indexOf(Math.max(...this.predictions));
    });
  }

  public clear(): void {
    this.canvas.clear();
    this.predictedNumber = null;
  }
}
