import {Directive, ElementRef, EventEmitter, HostListener, OnInit, Output} from '@angular/core';

interface Position {
  x: number;
  y: number;
}

@Directive({
  selector: '[appDrawable]'
})
export class DrawableDirective implements OnInit {
  public pos: Position = {x: 0, y: 0};
  ctx: CanvasRenderingContext2D;
  canvas: HTMLCanvasElement;

  @Output() newImage: EventEmitter<ImageData> = new EventEmitter();

  constructor(private el: ElementRef) {
  }

  public ngOnInit(): void {
    this.canvas = this.el.nativeElement as HTMLCanvasElement;
    this.ctx = this.canvas.getContext('2d');
  }

  @HostListener('mouseup', ['$event'])
  private onUp(): void {
    this.newImage.emit(this.getImgData());
  }

  @HostListener('mouseenter', ['$event'])
  private onEnter(event: MouseEvent): void {
    this.setPosition(event);
  }

  @HostListener('mousedown', ['$event'])
  private onMove(event: MouseEvent): void {
    this.setPosition(event);
  }

  @HostListener('mousemove', ['$event'])
  private onDown(event: MouseEvent): void {

    if (event.buttons !== 1) {
      return;
    }

    this.ctx.beginPath();

    this.ctx.lineWidth = 10;
    this.ctx.lineCap = 'round';
    this.ctx.strokeStyle = '#111111';

    this.ctx.moveTo(this.pos.x, this.pos.y);
    this.setPosition(event);
    this.ctx.lineTo(this.pos.x, this.pos.y);

    this.ctx.stroke();
  }

  @HostListener('resize', ['$event'])
  private onResize(): void {
    this.ctx.canvas.width = window.innerWidth;
    this.ctx.canvas.height = window.innerHeight;
  }

  setPosition(event: MouseEvent): void {
    this.pos.x = event.offsetX;
    this.pos.y = event.offsetY;
  }

  public clear(): void {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
  }

  getImgData(): ImageData {
    this.ctx.drawImage(this.canvas, 0, 0, 28, 28);
    return this.ctx.getImageData(0, 0, 28, 28);
  }
}
