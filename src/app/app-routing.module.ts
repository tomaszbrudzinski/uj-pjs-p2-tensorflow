import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BasicComponent} from './+basic/basic.component';
import {CameraComponent} from './+camera/camera.component';
import {DigitsComponent} from './+digits/digits.component';


const routes: Routes = [
  { path: 'basic', component: BasicComponent},
  { path: 'camera', component: CameraComponent},
  { path: 'digits', component: DigitsComponent},
  {path: '**', redirectTo: 'basic'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
