import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as mobilenet from '@tensorflow-models/mobilenet';
import {MobileNet} from '@tensorflow-models/mobilenet';
import * as tf from '@tensorflow/tfjs';

export interface Prediction {
  className: string;
  probability: number;
}

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.scss']
})
export class CameraComponent implements OnInit, AfterViewInit {

  @ViewChild('video', {static: false}) video: ElementRef;
  public predictions: Prediction[] = [{} as any, {} as any, {} as any];
  public loading: boolean = true;
  public tableColumns: string[] = ['index', 'class', 'probability'];
  private model: MobileNet;

  public async ngOnInit(): Promise<void> {
    this.model = await mobilenet.load();
    this.loading = false;

    setInterval(async () => {
      this.predictions = await this.model.classify(this.video.nativeElement);
      await tf.nextFrame();
    }, 3000);
  }

  public async ngAfterViewInit(): Promise<void> {
    this.setCamerStream(this.video.nativeElement);
  }

  private setCamerStream(videoElement: HTMLVideoElement): void {
    if (navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({video: true})
        .then((stream: MediaStream) => {
          videoElement.srcObject = stream;
        })
        .catch((error: Error) => {
          console.error(error);
        });
    }
  }
}
