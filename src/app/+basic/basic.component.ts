import {Component, OnInit} from '@angular/core';
import * as tf from '@tensorflow/tfjs';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['../shared/styles/classifier.scss']
})
export class BasicComponent implements OnInit {

  public prediction: number;
  private linearModel: tf.Sequential;

  public ngOnInit(): void {
    this.trainModel();
  }

  public async trainModel(): Promise<void> {

    this.linearModel = tf.sequential();
    this.linearModel.add(tf.layers.dense({units: 1, inputShape: [1]}));
    this.linearModel.compile({loss: 'meanSquaredError', optimizer: 'sgd'});

    const xs: tf.Tensor1D = tf.tensor1d([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    const ys: tf.Tensor1D = tf.tensor1d([2, 4, 6, 8, 10, 12, 14, 16, 18, 20]);

    await this.linearModel.fit(xs, ys);
  }

  private linearPrediction(event: any): void {
    const output: tf.Tensor = this.linearModel.predict(tf.tensor2d([parseFloat(event.target.value)], [1, 1])) as any;
    this.prediction = Array.from(output.dataSync())[0];
  }
}
